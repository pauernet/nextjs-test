import { server } from "../config";
import ArticleList from "../components/ArticleList";

export default function Home({ articles }) {
  return (
    <div>
      <ArticleList articles={articles} />
    </div>
  );
}

export async function getStaticProps() {
  let res = await fetch(`${server}/data.json`);
  const articles = await res.json();

  return {
    props: {
      articles,
    },
  };
}
